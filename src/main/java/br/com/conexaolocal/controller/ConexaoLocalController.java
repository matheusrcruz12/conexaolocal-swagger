package br.com.conexaolocal.controller;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.conexaolocal.entidade.Hello;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/api")
@Api(value="API ConexaoLocal")
public class ConexaoLocalController {
	 
	
	
	@GetMapping
	@ApiOperation(value="Retorna Hello World")
	public Hello homePage() {
		Hello hello = new Hello();	
		hello.setTitulo("Hello World");
	    return hello;
    }

}
